#Justin Henn

import numpy as np
import matplotlib.pyplot as plt 
import pandas as pd  
from sklearn.datasets import load_boston
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
from sklearn.metrics import r2_score
from sklearn.preprocessing import StandardScaler
import seaborn as sns 

#load dataset and set the target

dataset = load_boston()
boston = pd.DataFrame(dataset.data, columns=dataset.feature_names)
boston['MEDV'] = dataset.target

#correlate features to the target and plot heatmap

corr_matrix = boston.corr().round(2)
sns.heatmap(data=corr_matrix, annot=True)

plt.figure(figsize=(20, 5))

#pick features and load them into X and Y sets and create train/test sets

X = pd.DataFrame(np.c_[boston['LSTAT'], boston['RM']], columns = ['LSTAT','RM'])
y = boston.iloc[:, -1].values
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = .35, random_state = 0)


#scale features for X sets

sc = StandardScaler()
X_train = sc.fit_transform(X_train)
X_test = sc.transform(X_test)

#create simple linear regressor from training set

linear_regressor = LinearRegression()
linear_regressor.fit(X_train, y_train)

#predict y on test set

y_pred = linear_regressor.predict(X_test)

#find the mse and r2 of the prediction of the test set

mse = mean_squared_error(y_test, y_pred)
print(mse)
r2 = r2_score(y_test, y_pred)
print(r2)

#visualize some the actual vs predicted values

df1 = pd.DataFrame({'Predicted': y_pred , 'Actual': y_test})
df1 = df1.tail(50)
df1.plot.bar()